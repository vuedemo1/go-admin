set @@foreign_key_checks=OFF;
-- ----------------------------
-- Table structure for menu
-- ----------------------------
DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu` (
`id` int(11) NOT NULL AUTO_INCREMENT,
`path` varchar(64) DEFAULT NULL,
`name` varchar(64) DEFAULT NULL,
`name_zh` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
`icon` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
`component` varchar(64) DEFAULT NULL,
`parent_id` int(11) DEFAULT NULL,
PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- ----------------------------
-- Records of menu
-- ----------------------------
INSERT INTO `menu` VALUES ('1', '/admin/dashboard', 'Dashboard0', '首页', 'dashboard', 'AdminIndex', '0');
INSERT INTO `menu` VALUES ('2', '/admin/system', '系统管理', '系统管理', 'global', 'AdminIndex', '0');
INSERT INTO `menu` VALUES ('3', '/admin/task', '定时任务', '定时任务', 'clock-circle', 'AdminIndex', '0');
INSERT INTO `menu` VALUES ('21', '/admin/system/user', '用户管理', '用户管理', 'user', 'system/user/index', '2');
INSERT INTO `menu` VALUES ('22', '/admin/system/role', '角色管理', '角色管理', 'team', 'system/role/index', '2');
INSERT INTO `menu` VALUES ('23', '/admin/system/department', '部门管理', '部门管理', 'apartment', 'system/department/index', '2');
INSERT INTO `menu` VALUES ('24', '/admin/system/loginlog', '登录日志', '登录日志', 'file-text', 'system/loginlog/index', '2');
INSERT INTO `menu` VALUES ('31', '/admin/task/mission', '定时任务', '定时任务', 'switcher', 'task/Mission', '3');
INSERT INTO `menu` VALUES ('32', '/admin/task/test', 'test', 'test', 'switcher', 'task/Mission', '3');

-- ------------------------------
-- Table structure for permission
-- ------------------------------
DROP TABLE IF EXISTS `permission`;
CREATE TABLE `permission` (
`id` int(11) NOT NULL AUTO_INCREMENT,
`name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
`desc` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
`url` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of permission
-- ----------------------------
INSERT INTO `permission` VALUES ('1', 'users_management', '用户管理', '/api/system/user');
INSERT INTO `permission` VALUES ('2', 'roles_management', '角色管理', '/api/system/role');
INSERT INTO `permission` VALUES ('3', 'department_management', '部门管理', '/api/system/department');

-- ----------------------------
-- Table structure for role
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
`id` int(11) NOT NULL AUTO_INCREMENT,
`name` varchar(255) DEFAULT NULL,
`name_zh` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
`enabled` tinyint(1) DEFAULT NULL,
`remark` varchar(255) DEFAULT NULL,
`role_sort` int(4) DEFAULT NULL,
`create_time` timestamp NULL DEFAULT NULL,
`update_time` timestamp NULL DEFAULT NULL,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of role
-- ----------------------------
INSERT INTO `role` (name, name_zh, enabled,  remark, role_sort, create_time, update_time) VALUES
('sysAdmin', '系统管理员', '1', '', 0, '2020-08-19 01:00:19', '2020-08-19 01:00:19'),
('user', '普通用户', '1', '', 0, '2020-08-19 01:00:19', '2020-08-19 01:00:19'),
('visitor', '访客', '1', '', 0, '2020-08-19 01:00:19', '2020-08-19 01:00:19'),
('testuser', '测试用户', '1', '', 0, '2020-08-19 01:00:19', '2020-08-19 01:00:19');

-- ----------------------------
-- Table structure for role_menu
-- ----------------------------
DROP TABLE IF EXISTS `role_menu`;
CREATE TABLE `role_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rid` int(11) DEFAULT NULL,
  `mid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of role_menu
-- ----------------------------
INSERT INTO `role_menu` VALUES ('1', '1', '1');
INSERT INTO `role_menu` VALUES ('2', '1', '2');
INSERT INTO `role_menu` VALUES ('3', '1', '3');
INSERT INTO `role_menu` VALUES ('4', '2', '1');
INSERT INTO `role_menu` VALUES ('5', '2', '2');
INSERT INTO `role_menu` VALUES ('6', '3', '1');
INSERT INTO `role_menu` VALUES ('7', '4', '2');
INSERT INTO `role_menu` VALUES ('8', '4', '1');

-- ----------------------------
-- Table structure for role_permission
-- ----------------------------
DROP TABLE IF EXISTS `role_permission`;
CREATE TABLE `role_permission` (
`id` int(20) NOT NULL AUTO_INCREMENT,
`rid` int(20) DEFAULT NULL,
`pid` int(20) DEFAULT NULL,
PRIMARY KEY (`id`),
KEY `fk_role_permission_role_1` (`rid`),
KEY `fk_role_permission_permission_1` (`pid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of role_permission
-- ----------------------------
INSERT INTO `role_permission` VALUES ('1', '1', '1');
INSERT INTO `role_permission` VALUES ('2', '1', '2');
INSERT INTO `role_permission` VALUES ('3', '1', '3');
INSERT INTO `role_permission` VALUES ('4', '2', '3');
INSERT INTO `role_permission` VALUES ('5', '3', '3');
INSERT INTO `role_permission` VALUES ('6', '4', '3');

-- ----------------------------
-- Table structure for sex
-- ----------------------------
DROP TABLE IF EXISTS `sex`;
CREATE TABLE `sex` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `value` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sex
-- ----------------------------
INSERT INTO `sex` (value) VALUES
('男'),
('女'),
('未知');

-- ----------------------------
-- Table structure for department
-- ----------------------------
DROP TABLE IF EXISTS `department`;
CREATE TABLE `department` (
   `id` int(11) NOT NULL AUTO_INCREMENT,
   `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
   `dept_key` varchar(255) DEFAULT NULL,
   `dept_sort` int(4) DEFAULT NULL,
#    `uid` int(11) DEFAULT NULL,
   `phone` varchar(11) DEFAULT NULL,
   `email` varchar(64) DEFAULT NULL,
   `enabled` int(1) DEFAULT NULL,
   `create_time` timestamp NULL DEFAULT NULL,
   `update_time` timestamp NULL DEFAULT NULL,
   `parent_id` int(11) DEFAULT NULL,
   PRIMARY KEY (`id`)
#    KEY `user_on_uid` (`uid`),
#    CONSTRAINT `user_on_uid` FOREIGN KEY (`uid`) REFERENCES `user` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of department
-- ----------------------------
INSERT INTO `department` VALUES
# ('1', '金航数码', '0-0', '0', null, '18811112212', 'jhsm@avic.com', '1', '2020-08-19 01:00:19', '2020-08-19 01:00:19', '0'),
# ('2', '研发中心', '0-0-1', '1', '1', '13432344459', 'yfax@avic.com', '1', '2020-08-19 01:00:19', '2020-08-19 01:00:19', '1'),
# ('3', '质量管理部', '0-0-2', '1', '2', '18966579234', 'zlglb@avic.com', '1', '2020-08-19 01:00:19', '2020-08-19 01:00:19', '1'),
# ('4', '系统集成部', '0-0-3', '1', '2', '18966579234', 'xtjcb@avic.com', '0', '2020-08-19 01:00:19', '2020-08-19 01:00:19', '1'),
# ('5', '运行管理中心', '0-0-4', '1', '2', '18966579234', 'yxglzx@avic.com', '1', '2020-08-19 01:00:19', '2020-08-19 01:00:19', '1'),
# ('6', '测试部门', '0-1', '1', '3', '18966579234', 'csbm@test.com', '1', '2020-08-19 01:00:19', '2020-08-19 01:00:19', '0'),
# ('7', '硬件研发部', '0-1-1', '1', '3', '18966579234', 'yjyfb@test.com', '1', '2020-08-19 01:00:19', '2020-08-19 01:00:19', '6');
('1', '金航数码', '0-0', 0, '18811112212', 'jhsm@avic.com', '1', '2020-08-19 01:00:19', '2020-08-19 01:00:19', 0),
('2', '研发中心', '0-0-1', 1, '13432344459', 'yfax@avic.com', '1', '2020-08-19 01:00:19', '2020-08-19 01:00:19', 1),
('3', '质量管理部', '0-0-2', 1, '18966579234', 'zlglb@avic.com', '1', '2020-08-19 01:00:19', '2020-08-19 01:00:19', 1),
('4', '系统集成部', '0-0-3', 1, '18966579234', 'xtjcb@avic.com', '0', '2020-08-19 01:00:19', '2020-08-19 01:00:19', 1),
('5', '运行管理中心', '0-0-4', 1, '18966579234', 'yxglzx@avic.com', '1', '2020-08-19 01:00:19', '2020-08-19 01:00:19', 1),
('6', '外协公司', '0-1', 1,  '18966579234', 'csbm@test.com', '1', '2020-08-19 01:00:19', '2020-08-19 01:00:19', 0),
('7', '软件开发部', '0-1-1', 1, '18966579234', 'yjyfb@test.com', '1', '2020-08-19 01:00:19', '2020-08-19 01:00:19', 6);

-- ----------------------------
-- Table structure for postion
-- ----------------------------
DROP TABLE IF EXISTS `postname`;
CREATE TABLE `postname` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of position
-- ----------------------------
INSERT INTO `postname` (name) VALUES
('软件开发工程师'),
('测试工程师'),
('系统集成工程师'),
('运维工程师');

-- ----------------------------
-- Table structure for user_role
-- ----------------------------
DROP TABLE IF EXISTS `user_role`;
CREATE TABLE `user_role` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `uid` int(11) DEFAULT NULL,
 `rid` int(11) DEFAULT NULL,
 PRIMARY KEY (`id`),
 KEY `fk_operator_role_operator_1` (`uid`),
 KEY `fk_operator_role_role_1` (`rid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user_role
-- ----------------------------
INSERT INTO `user_role` (uid, rid) VALUES
('1', '1'),
('2', '2'),
('2', '4'),
('3', '3'),
('4', '4'),
('5', '4'),
('6', '4'),
('7', '4'),
('8', '4'),
('9', '4');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
   `id` int(11) NOT NULL AUTO_INCREMENT,
   `username` char(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
   `nickname` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
   `password` varchar(255) DEFAULT NULL,
   `salt` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
   `sexid` int(11) DEFAULT NULL,
   `phone` varchar(255) DEFAULT '',
   `email` varchar(255) DEFAULT '',
   `did` int(11) DEFAULT NULL,
   `pid` int(11) DEFAULT NULL,
   `enabled` tinyint(1) DEFAULT NULL,
   `remark` varchar(255) DEFAULT '',
   `avatar` varchar(255) DEFAULT '',
   `create_time` timestamp NULL DEFAULT NULL,
   `update_time` timestamp NULL DEFAULT NULL,
   PRIMARY KEY (`id`),
   KEY `sex_on_sid` (`sexid`),
   CONSTRAINT `sex_on_sid` FOREIGN KEY (`sexid`) REFERENCES `sex` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
   KEY `department_on_did` (`did`),
   CONSTRAINT `department_on_did` FOREIGN KEY (`did`) REFERENCES `department` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
   KEY `postname_on_pid` (`pid`),
   CONSTRAINT `postname_on_pid` FOREIGN KEY (`pid`) REFERENCES `postname` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user admin/admin  guo/guo  visitor/visitor  testX/test
-- ----------------------------
INSERT INTO `user` (username, nickname, password, salt, sexid, phone, email, did, pid, enabled, remark, avatar, create_time, update_time) VALUES
('admin', '管理员', '9fdf6694256a039b685e1ba0145e1765', 'Ds3FYHxnUsosQG/rciueWg==', '1', '18811372815', 'mm768528@163.com', '1', '1', '1','', 'http://localhost:8443/api/file/logo-20210423134638.jpg', '2020-08-19 01:00:19', '2020-08-19 01:00:19'),
('guo', 'Adeline', '9615f48df3cd388e338e9e1405be868f', '7N56C4dPD5nrjS0ABTgrbQ==', '2', '18996123244', 'test@163.com', '2', '2', '0', '', 'http://localhost:8443/api/file/avatar1-20210426151743.jpg', '2020-08-19 01:00:19', '2020-08-19 01:00:19'),
('visitor', '游客', 'a5aebd5e1e557891440e97df79fc72ca', 'HO8cwcN4BJSYQ+beG5Mmvg==', '2', '13323234566', 'visitor@test.com', '3', '2', '1', '', 'http://localhost:8443/api/file/avatar2-20210426151832.jpg', '2020-08-19 01:00:19', '2020-08-19 01:00:19'),
('test1', '测试用户1', '957691c2e90234bb2ab417985b85bcb8', 'WJ9AQDLKg50mxMZCDzipKA==', '1', '13456789000', 'test@test.com', '3', '3', '1', '', 'http://localhost:8443/api/file/defaultAvatar.jpg', '2020-08-19 01:00:19', '2020-08-19 01:00:19'),
('test2', '测试用户2', '957691c2e90234bb2ab417985b85bcb8', 'WJ9AQDLKg50mxMZCDzipKA==', '1', '13456789000', 'test@test.com', '5', '4', '1', '', 'http://localhost:8443/api/file/defaultAvatar.jpg', '2020-08-19 01:00:19', '2020-08-19 01:00:19'),
('test3', '测试用户3', '957691c2e90234bb2ab417985b85bcb8', 'WJ9AQDLKg50mxMZCDzipKA==', '2', '13456789000', 'test@test.com', '2', '3', '1', '', 'http://localhost:8443/api/file/defaultAvatar.jpg', '2020-08-19 01:00:19', '2020-08-19 01:00:19'),
('test4', '测试用户4', '957691c2e90234bb2ab417985b85bcb8', 'WJ9AQDLKg50mxMZCDzipKA==', '1', '13456789000', 'test@test.com', '6', '4', '1', '', 'http://localhost:8443/api/file/defaultAvatar.jpg', '2020-08-19 01:00:19', '2020-08-19 01:00:19'),
('test5', '测试用户5', '957691c2e90234bb2ab417985b85bcb8', 'WJ9AQDLKg50mxMZCDzipKA==', '1', '13456789000', 'test@test.com', '7', '2', '1', '', 'http://localhost:8443/api/file/defaultAvatar.jpg', '2020-08-19 01:00:19', '2020-08-19 01:00:19'),
('test6', '测试用户6', '957691c2e90234bb2ab417985b85bcb8', 'WJ9AQDLKg50mxMZCDzipKA==', '1', '13456789000', 'test@test.com', '4', '3', '1', '', 'http://localhost:8443/api/file/defaultAvatar.jpg', '2020-08-19 01:00:19', '2020-08-19 01:00:19');

-- ----------------------------
-- Table structure for loginlog
-- ----------------------------
DROP TABLE IF EXISTS `loginlog`;
CREATE TABLE `loginlog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(128) DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  `ipaddr` varchar(255) DEFAULT NULL,
  `login_location` varchar(255) DEFAULT NULL,
  `browser` varchar(255) DEFAULT NULL,
  `os` varchar(255) DEFAULT NULL,
  `login_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `msg` varchar(255) DEFAULT NULL,
  `create_time` timestamp NULL DEFAULT NULL,
  `update_time` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=147 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of loginlog
-- ----------------------------
