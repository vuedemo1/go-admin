package com.adeline.goadmin.dao;

import com.adeline.goadmin.pojo.Loginlog;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface LoginlogDAO extends JpaRepository<Loginlog, Integer> {
    List<Loginlog> findAll();
    List<Loginlog> findAllByIpaddrLikeAndUsernameLike(String ipaddr, String username);
    void deleteAll();
    void deleteById(int id);
}
