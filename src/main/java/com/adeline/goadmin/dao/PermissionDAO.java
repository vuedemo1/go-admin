package com.adeline.goadmin.dao;

import com.adeline.goadmin.pojo.Permission;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PermissionDAO extends JpaRepository<Permission, Integer> {
    Permission findById(int id);
}
