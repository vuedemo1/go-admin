package com.adeline.goadmin.dao;

import com.adeline.goadmin.pojo.RoleMenu;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface RoleMenuDAO extends JpaRepository<RoleMenu, Integer> {
    List<RoleMenu> findAllByRid(int rid);
    List<RoleMenu> findAllByRidIn(List<Integer> rids);
}
