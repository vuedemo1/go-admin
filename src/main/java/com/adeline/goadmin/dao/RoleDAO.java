package com.adeline.goadmin.dao;

import com.adeline.goadmin.pojo.Role;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface RoleDAO extends JpaRepository<Role, Integer> {
    Role findById(int id);
    Role findByName(String name);

    List<Role> findAllByNameLikeAndNameZhLike(String name, String nameZh);
}
