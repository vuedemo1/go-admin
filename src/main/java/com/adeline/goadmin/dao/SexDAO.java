package com.adeline.goadmin.dao;

import com.adeline.goadmin.pojo.Sex;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SexDAO extends JpaRepository<Sex, Integer> {
}
