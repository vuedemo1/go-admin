package com.adeline.goadmin.dao;

import com.adeline.goadmin.pojo.UserRole;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UserRoleDAO extends JpaRepository<UserRole, Integer> {
    List<UserRole> findAllByUid(int uid);
    void deleteAllByUid(int uid);
}
