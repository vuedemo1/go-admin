package com.adeline.goadmin.dao;

import com.adeline.goadmin.pojo.Postname;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PostnameDAO extends JpaRepository<Postname, Integer> {
}
