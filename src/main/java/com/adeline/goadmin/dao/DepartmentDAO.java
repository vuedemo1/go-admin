package com.adeline.goadmin.dao;

import com.adeline.goadmin.pojo.Department;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DepartmentDAO extends JpaRepository<Department, Integer> {
    Department findById(int id);
    Department findByName(String name);
    List<Department> findAllByParentId(int parentId);
    List<Department> findAllByNameLike(String name);
}
