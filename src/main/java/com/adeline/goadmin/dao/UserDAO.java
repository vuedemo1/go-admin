package com.adeline.goadmin.dao;

import com.adeline.goadmin.pojo.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UserDAO extends JpaRepository<User, Integer> {
    User findByUsername(String username);

    User getByUsernameAndPassword(String username, String password);

    User findById(int id);

    List<User> findAllByDepartmentId(int deptId);

    List<User> findAllByUsernameLikeAndPhoneLike(String username, String phone);
}
