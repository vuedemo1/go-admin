package com.adeline.goadmin.dao;

import com.adeline.goadmin.pojo.RolePermission;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface RolePermissionDAO extends JpaRepository<RolePermission, Integer> {

List<RolePermission> findAllByRid(int rid);
List<RolePermission> findAllByRidIn(List<Integer> rids);
}
