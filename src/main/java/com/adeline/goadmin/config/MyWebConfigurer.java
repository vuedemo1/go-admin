package com.adeline.goadmin.config;

import com.adeline.goadmin.bean.Avatar;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@SpringBootConfiguration
@EnableConfigurationProperties({Avatar.class})
public class MyWebConfigurer implements WebMvcConfigurer {
    @Autowired
    private Avatar avatar;

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        // 所有请求都允许跨域
        // allowCredentials(true) 允许跨域使用 cookie 的情况下，allowedOrigins() 不能使用通配符 *
        registry.addMapping("/**")
                .allowCredentials(true)
                .allowedOrigins("http://localhost:8080")
                .allowedMethods("POST", "GET", "PUT", "OPTIONS", "DELETE")
                .allowedHeaders("*");
    }

    /**
     * 添加静态资源文件，外部可以直接访问地址
     * @param registry
     */
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/api/file/**").addResourceLocations("file:" + avatar.getPath());
    }

}
