package com.adeline.goadmin.controller;

import com.adeline.goadmin.pojo.User;
import com.adeline.goadmin.result.Result;
import com.adeline.goadmin.result.ResultFactory;
import com.adeline.goadmin.service.LoginlogService;
import com.adeline.goadmin.service.UserService;
import org.apache.http.auth.AuthenticationException;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.HtmlUtils;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Map;

@RestController
public class LoginController {

    @Autowired
    UserService userService;
    @Autowired
    LoginlogService loginlogService;

    @CrossOrigin
    @PostMapping("/api/login")
    @ResponseBody
    public Result login(HttpServletRequest request) throws IOException {
//        老方法：不使用shiro
 /*       // 对 html 标签进行转义， 防止 XSS 攻击
        String username = requestUser.getUsername();
        username = HtmlUtils.htmlEscape(username);

        User user = userService.get(username, requestUser.getPassword());
        if (null != user) {
            String succMessage = "登录成功！";
            System.out.println(succMessage);
            return ResultFactory.buildSuccessResult(username);
        } else {
            String failMessage = "账号或密码错误！";
            System.out.println(failMessage);
            return ResultFactory.buildFailResult(failMessage);
        }*/
        String username = request.getParameter("username");
        Subject subject = SecurityUtils.getSubject();
        UsernamePasswordToken usernamePasswordToken = new UsernamePasswordToken(username, request.getParameter("password"));
        usernamePasswordToken.setRememberMe(true);
        // 登录信息
        String message = "";
        // 用户是否成功登录
        Boolean status = false;
        try {
            subject.login(usernamePasswordToken);
            status = true;
            message = "登录成功";
            System.out.println(message);
            return ResultFactory.buildSuccessResult(username);
        } catch (IncorrectCredentialsException e) {
            message = "密码错误";
            return ResultFactory.buildFailResult(message);
        } catch (UnknownAccountException e) {
            message = "用户不存在";
            return ResultFactory.buildFailResult(message);
        } finally {
            loginlogService.writeLoginlogInDB(request, username, message, status);
        }
    }

    @CrossOrigin
    @GetMapping("/api/logout")
    @ResponseBody
    public Result logout() {
        Subject subject = SecurityUtils.getSubject();
        subject.logout();
        String succMessage = "成功登出";
        System.out.println(succMessage);
        return ResultFactory.buildSuccessResult(succMessage);
    }

    @CrossOrigin
    @GetMapping("/api/authentication")
    @ResponseBody
    public String authentication() {
        return "身份认证成功";
    }

    /**
     * 列出所有登录日志
     * @return
     */
    @CrossOrigin
    @GetMapping("/api/system/loginlog")
    @ResponseBody
    public Result listLoginlog() {
        return ResultFactory.buildSuccessResult(loginlogService.listLoginlogs());
    }

    /**
     * 查询按钮：按登录IP或用户名称查询日志
     * @param ipaddr
     * @param username
     * @return
     */
    @CrossOrigin
    @GetMapping("/api/system/loginlog/search")
    @ResponseBody
    public Result search(@RequestParam(value = "ipaddr") String ipaddr, @RequestParam(value = "username") String username) {
        if ("".equals(ipaddr) && "".equals(username)) {
            return ResultFactory.buildSuccessResult(loginlogService.listLoginlogs());
        } else {
            return ResultFactory.buildSuccessResult(loginlogService.search(ipaddr, username));
        }
    }

    /**
     * 删除登录日志 / 批量删除
     * @param params
     * @return
     */
    @CrossOrigin
    @PostMapping("/api/system/loginlog/delete")
    public Result deleteById(@RequestBody Map<String,Object> params) {
        // 去掉字符串中的空格
        String selectedIds = params.get("selectedIds").toString().replaceAll("\\s*", "");
        // 表格操作列删除操作，删除一项
        if(!selectedIds.startsWith("[")) {
            loginlogService.deleteById(Integer.parseInt(selectedIds));
        } else {
            // 批量删除，去掉字符串前后"[]"符号
            String newSelectedIds = selectedIds.substring(1, selectedIds.length()-1);
            String[] ids = newSelectedIds.split(",");
            for (String id : ids) {
                    loginlogService.deleteById(Integer.parseInt(id));
            }
        }
        return ResultFactory.buildSuccessResult("登录日志删除成功");
    }

    /**
     * 清空所有登录日志
     * @return
     */
    @CrossOrigin
    @PostMapping("/api/system/loginlog/deleteAll")
    public Result deleteAll() {
        loginlogService.deleteAll();
        return ResultFactory.buildSuccessResult("成功清空所有数据项");
    }

}
