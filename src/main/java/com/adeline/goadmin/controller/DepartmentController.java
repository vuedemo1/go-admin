package com.adeline.goadmin.controller;

import com.adeline.goadmin.pojo.Department;
import com.adeline.goadmin.result.Result;
import com.adeline.goadmin.result.ResultFactory;
import com.adeline.goadmin.service.DepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class DepartmentController {
    @Autowired
    DepartmentService departmentService;

    @CrossOrigin
    @GetMapping("/api/system/department")
    @ResponseBody
    public Result listDepartmentTree() { return ResultFactory.buildSuccessResult(departmentService.listDepartmentTree());
    }

    /**
     * 按id查询部门
     * @param id
     * @return
     */
    @CrossOrigin
    @GetMapping("/api/system/dept/{id}")
    @ResponseBody
    public Result getDeptById(@PathVariable("id") int id) {
        return ResultFactory.buildSuccessResult(departmentService.getDepartmentById(id));
    }

    /**
     * 按 id 删除部门
     * @param id
     * @return
     */
    @CrossOrigin
    @PostMapping("/api/system/dept/delete/{id}")
    @ResponseBody
    public Result deleteDeptById(@PathVariable("id") int id) {
        departmentService.deleteById(id);
        return ResultFactory.buildSuccessResult("部门删除成功");
    }

    /**
     * 修改部门信息
     * @param requestDept
     * @return
     */
    @CrossOrigin
    @PutMapping("/api/system/dept")
    @ResponseBody
    public Result editDepartment(@RequestBody Department requestDept) {
        departmentService.editDept(requestDept);
        return ResultFactory.buildSuccessResult("修改成功");
    }

    /**
     * 新增部门
     * @param requestDept
     * @return
     */
    @CrossOrigin
    @PostMapping("/api/system/dept")
    @ResponseBody
    public Result addDepartment(@RequestBody Department requestDept) {
        int status = departmentService.addDept(requestDept);
        switch (status) {
            case 0:
                return ResultFactory.buildFailResult("部门名称不能为空");
            case 1:
                return ResultFactory.buildSuccessResult("添加成功");
            case 2:
                return ResultFactory.buildFailResult("部门名称已存在");
        }
        return ResultFactory.buildFailResult("未知错误");
    }

    /**
     * 查询按钮：按部门名称查询
     * @param deptName
     * @return
     */
    @CrossOrigin
    @GetMapping("/api/system/dept/search")
    @ResponseBody
    public Result search(@RequestParam(value = "name") String deptName) {
        if ("".equals(deptName)) {
            return ResultFactory.buildSuccessResult(departmentService.listDepartmentTree());
        } else {
            return ResultFactory.buildSuccessResult(departmentService.search(deptName));
        }
    }
}
