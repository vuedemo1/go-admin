package com.adeline.goadmin.controller;

import com.adeline.goadmin.pojo.Role;
import com.adeline.goadmin.result.Result;
import com.adeline.goadmin.result.ResultFactory;
import com.adeline.goadmin.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RestController
public class RoleController {

    @Autowired
    RoleService roleService;

    /**
     * 列出所有角色
     * @return
     */
    @CrossOrigin
    @GetMapping("/api/system/role")
    @ResponseBody
    public Result listRoles() {
        return ResultFactory.buildSuccessResult(roleService.listRoles());
    }

    /**
     * 按id查询角色
     * @param id
     * @return
     */
    @CrossOrigin
    @GetMapping("/api/system/role/{id}")
    @ResponseBody
    public Result getRoleById(@PathVariable("id") int id) {
        return ResultFactory.buildSuccessResult(roleService.findById(id));
    }

    /**
     * 增加角色
     * @param requestRole
     * @return
     */
    @CrossOrigin
    @PostMapping("/api/system/role")
    @ResponseBody
    public Result addRole(@RequestBody Role requestRole) {
        int status = roleService.addRole(requestRole);
        switch (status) {
            case 0:
                return ResultFactory.buildFailResult("角色名称和权限字符不能为空");
            case 1:
                return ResultFactory.buildSuccessResult("添加成功");
            case 2:
                return ResultFactory.buildFailResult("角色已存在");
        }
        return ResultFactory.buildFailResult("未知错误");
    }

    /**
     * 修改角色信息
     * @param requestRole
     * @return
     */
    @CrossOrigin
    @PutMapping("/api/system/role")
    @ResponseBody
    public Result editRole(@RequestBody Role requestRole) {
        roleService.editRole(requestRole);
        return ResultFactory.buildSuccessResult("修改成功");
    }

    /**
     * 更新角色状态（正常 / 停用）
     * @param requestRole
     * @return
     */
    @CrossOrigin
    @PutMapping("/api/system/role/status")
    @ResponseBody
    public Result updateRoleStatus(@RequestBody @Valid Role requestRole) {
        roleService.updateRoleStatus(requestRole);
        return ResultFactory.buildSuccessResult("角色状态更新成功");
    }

    /**
     * 删除用户 / 批量删除
     * @param params
     * @return
     */
    @CrossOrigin
    @PostMapping("/api/system/role/delete")
    public Result deleteRole(@RequestBody Map<String,Object> params) {
        // 去掉字符串中的空格
        String selectedIds = params.get("selectedIds").toString().replaceAll("\\s*", "");
        // 表格操作列删除操作，删除一项
        if(!selectedIds.startsWith("[")) {
            roleService.deleteById(Integer.parseInt(selectedIds));
        } else {
        // 批量删除，去掉字符串前后"[]"符号
            String newSelectedIds = selectedIds.substring(1, selectedIds.length()-1);
            String[] ids = newSelectedIds.split(",");
            for (String id : ids) {
                if (Integer.parseInt(id) == 1) {
                    return ResultFactory.buildFailResult("不能删除管理员角色");
                } else {
                    roleService.deleteById(Integer.parseInt(id));
                }
            }
        }
        return ResultFactory.buildSuccessResult("角色删除成功");
    }

    /**
     * 按id列表查询
     * @param params
     * @return
     */
    @CrossOrigin
    @PostMapping("/api/system/role/ids")
    public Result findRolesByIds(@RequestBody Map<String,Object> params) {
        List<Role> roles = new ArrayList<Role>();
        // 去掉字符串中的空格
        String selectedIds = params.get("selectedIds").toString().replaceAll("\\s*", "");
        // 批量查询，去掉字符串前后"[]"符号
        String newSelectedIds = selectedIds.substring(1, selectedIds.length()-1);
        String[] ids = newSelectedIds.split(",");
        for (String id : ids) {
            roles.add(roleService.findById(Integer.parseInt(id)));
        }

        return ResultFactory.buildSuccessResult(roles);
    }

    /**
     * 查询按钮：按角色名称和（或）权限字符查询
     * @param name 权限字符
     * @param nameZh 角色名称
     * @return
     */
    @CrossOrigin
    @GetMapping("/api/system/role/search")
    @ResponseBody
    public Result search(@RequestParam(value = "name") String name, @RequestParam(value = "nameZh") String nameZh) {
        if ("".equals(name) && "".equals(nameZh)) {
            return ResultFactory.buildSuccessResult(roleService.listRoles());
        } else {
            return ResultFactory.buildSuccessResult(roleService.search(name, nameZh));
        }
    }
}
