package com.adeline.goadmin.controller;

import com.adeline.goadmin.result.Result;
import com.adeline.goadmin.result.ResultFactory;
import com.adeline.goadmin.service.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MenuController {
    @Autowired
    MenuService menuService;

    @CrossOrigin
    @GetMapping("/api/menu")
    @ResponseBody
    public Result menu() { return ResultFactory.buildSuccessResult(menuService.getMenusByCurrentUser());
    }

    @GetMapping("/api/system/role/menu")
    public Result listAllMenus() {
        return ResultFactory.buildSuccessResult(menuService.getMenusByRoleId(1));
    }
}
