package com.adeline.goadmin.controller;

import com.adeline.goadmin.bean.Avatar;
import com.adeline.goadmin.pojo.User;
import com.adeline.goadmin.result.Result;
import com.adeline.goadmin.result.ResultFactory;
import com.adeline.goadmin.service.DepartmentService;
import com.adeline.goadmin.service.PostnameService;
import com.adeline.goadmin.service.SexService;
import com.adeline.goadmin.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

@RestController
@EnableConfigurationProperties({Avatar.class})
public class UserController {

    @Autowired
    UserService userService;
    @Autowired
    DepartmentService departmentService;
    @Autowired
    PostnameService postnameService;
    @Autowired
    SexService sexService;
    @Autowired
    Avatar avatar;

    /**
     * 列出所有用户
     * @return
     */
    @CrossOrigin
    @GetMapping("/api/system/user")
    @ResponseBody
    public Result listUser() {
        return ResultFactory.buildSuccessResult(userService.listUser());
    }

    /**
     * 按id查询用户
     * @param id
     * @return
     */
    @CrossOrigin
    @GetMapping("/api/system/user/{id}")
    @ResponseBody
    public Result getUserById(@PathVariable("id") int id) {
        return ResultFactory.buildSuccessResult(userService.findById(id));
    }

    /**
     * Get current user in DB.
     * @return
     */
    @CrossOrigin
    @GetMapping("/api/profile")
    @ResponseBody
    public Result getCurrentUser() {
        return ResultFactory.buildSuccessResult(userService.getCurrentUser());
    }

    /**
     * 新增用户
     * @param requestUser
     * @return
     */
    @CrossOrigin
    @PostMapping("/api/system/user")
    @ResponseBody
    public Result addUser(@RequestBody User requestUser) {
        int status = userService.add(requestUser);
        switch (status) {
            case 0:
                return ResultFactory.buildFailResult("用户名和密码不能为空");
            case 1:
                return ResultFactory.buildSuccessResult("添加成功");
            case 2:
                return ResultFactory.buildFailResult("用户已存在");
        }
        return ResultFactory.buildFailResult("未知错误");
    }

    /**
     * 修改用户信息
     * @param requestUser
     * @return
     */
    @CrossOrigin
    @PutMapping("/api/system/user")
    @ResponseBody
    public Result editUser(@RequestBody User requestUser) {
        int status = userService.editUser(requestUser);
        switch (status) {
            case 0:
                return ResultFactory.buildFailResult("用户名不能为空");
            case 1:
                return ResultFactory.buildSuccessResult("添加成功");
            case 2:
                return ResultFactory.buildFailResult("用户名已存在");
        }
        return ResultFactory.buildFailResult("未知错误");
    }

    /**
     * 更新用户状态（正常 / 停用）
     * @param requestUser
     * @return
     */
    @CrossOrigin
    @PutMapping("/api/system/user/status")
    @ResponseBody
    public Result updateUserStatus(@RequestBody @Valid User requestUser) {
        userService.updateUserStatus(requestUser);
        return ResultFactory.buildSuccessResult("用户状态更新成功");
    }

    @CrossOrigin
    @PutMapping("/api/system/user/password")
    @ResponseBody
    public Result resetPassword(@RequestBody @Valid User requestUser) {
        userService.resetPassword(requestUser);
        return ResultFactory.buildSuccessResult("重置密码成功");
    }

    /**
     * 删除用户 / 批量删除
     * @param params
     * @return
     */
    @CrossOrigin
    @PostMapping("/api/system/user/delete")
    public Result deleteUser(@RequestBody Map<String,Object> params) {
//        去掉字符串中的空格
        String selectedIds = params.get("selectedIds").toString().replaceAll("\\s*", "");
//        表格操作列删除操作，删除一项
        if(!selectedIds.startsWith("[")) {
            userService.deleteById(Integer.parseInt(selectedIds));
        } else {
//            批量删除，去掉字符串前后"[]"符号
            String newSelectedIds = selectedIds.substring(1, selectedIds.length()-1);
            String[] ids = newSelectedIds.split(",");
            for (String id : ids) {
                if (Integer.parseInt(id) == 1) {
                    return ResultFactory.buildFailResult("不能删除管理员账号");
                } else {
                    userService.deleteById(Integer.parseInt(id));
                }
            }
        }
        return ResultFactory.buildSuccessResult("用户删除成功");
    }

    /**
     * 列出所有性别选项
     * @return
     */
    @CrossOrigin
    @GetMapping("/api/system/user/sex")
    @ResponseBody
    public Result listSex() {
        return ResultFactory.buildSuccessResult(sexService.listSex());
    }

    /**
     * 列出所有部门选项
     * @return
     */
    @CrossOrigin
    @GetMapping("/api/system/user/department")
    @ResponseBody
    public Result listDepartment() {
        return ResultFactory.buildSuccessResult(departmentService.listDepartment());
    }

    /**
     * 列出所有岗位选项
     * @return
     */
    @CrossOrigin
    @GetMapping("/api/system/user/postname")
    @ResponseBody
    public Result listPostname() {
        return ResultFactory.buildSuccessResult(postnameService.listPostname());
    }

    /**
     * 点击部门树查询用户
     * @param deptId
     * @return
     */
    @CrossOrigin
    @GetMapping("/api/system/user/dept/{deptId}")
    @ResponseBody
    public Result findByDept(@PathVariable("deptId") int deptId) {
        return ResultFactory.buildSuccessResult(userService.searchByDeptId(deptId));
    }

    /**
     * 查询按钮：按用户名和（或）电话查询
     * @param username
     * @param phone
     * @return
     */
    @CrossOrigin
    @GetMapping("/api/system/user/search")
    @ResponseBody
    public Result search(@RequestParam(value = "username") String username, @RequestParam(value = "phone") String phone) {
        if ("".equals(username) && "".equals(phone)) {
            return ResultFactory.buildSuccessResult(userService.listUser());
        } else {
            return ResultFactory.buildSuccessResult(userService.search(username, phone));
        }
    }

    /**
     * 上传用户头像并更新数据库
     * @param file
     * @return
     * @throws Exception
     */
    @CrossOrigin
    @PostMapping("/api/avatar")
    public Result avatarUpload(MultipartFile file) throws Exception {
        String folder = avatar.getPath();
        File imageFolder = new File(folder);
        // 防止图片重名，在图片名称中加时间戳：name.jpg -> name-yyyyMMddHHmmss.jpg
        String timestamp = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
        File f = new File(imageFolder, file.getOriginalFilename()
                .substring(0, file.getOriginalFilename().length() - 4) + "-" + timestamp + file.getOriginalFilename()
                .substring(file.getOriginalFilename().length() - 4));
        if (!f.getParentFile().exists())
            f.getParentFile().mkdirs();
        try {
            file.transferTo(f);
            String imgURL = "http://localhost:8443/api/file/" + f.getName();
            User currentUser = userService.getCurrentUser();
            currentUser.setAvatar(imgURL);
            userService.updateUserAvatar(currentUser);
            return ResultFactory.buildSuccessResult(imgURL);
        } catch (IOException e) {
            e.printStackTrace();
            return ResultFactory.buildFailResult("上传失败！");
        }
    }
}
