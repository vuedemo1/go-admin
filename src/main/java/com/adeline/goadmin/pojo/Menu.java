package com.adeline.goadmin.pojo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "menu")
@ToString
@JsonIgnoreProperties({"handler", "hibernateLazyInitializer"})
public class Menu {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private int id;

    /**
     * Menu access path
     */
    private String path;

    /**
     * Menu name
     */
    private String name;

    /**
     * Menu name in Chinese
     */
    private String nameZh;

    /**
     * Menu icon class(use antd-vue icons).
     */
    private String icon;

    /**
     * Front-end component name corresponding to menu.
     */
    private String component;

    /**
     * Parent menu.
     */
    private int parentId;

    /**
     * Transient property for storing children menus.
     */
    @Transient
    private List<Menu> children;
}
