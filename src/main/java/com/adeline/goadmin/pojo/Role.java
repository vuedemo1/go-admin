package com.adeline.goadmin.pojo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Role entity.
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "role")
@ToString
@JsonIgnoreProperties({"handler", "hibernateLazyInitializer"})
public class Role {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    /**
     * Role name. 权限字符
     */
    private String name;

    /**
     * Role name in Chinese. 角色名称
     */
    @Column(name = "name_zh")
    private String nameZh;

    /**
     * Role status. 角色状态
     */
    private boolean enabled;

    /**
     * Role sort. 显示顺序
     */
    private int roleSort;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;

    /**
     * Remark 备注
     */
    private String remark;

    /**
     * Transient property for storing permissions owned by current role.
     */
    @Transient
    private List<Permission> perms;

    /**
     * Transient property for storing menus owned by current role.
     */
    @Transient
    private List<Menu> menus;
}
