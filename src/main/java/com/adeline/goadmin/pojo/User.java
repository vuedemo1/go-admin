package com.adeline.goadmin.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.Email;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * User entity
 *
 * @Data可以为类提供读写功能，从而不用写get、set方法。
 * @NoArgsConstructor 为类提供一个无参的构造方法。
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "user")
@ToString
@JsonIgnoreProperties({"handler", "hibernateLazyInitializer"})
public class User {
    @Id
//    主键由数据库生成, 采用数据库自增长, Oracle不支持这种方式
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    int id;

    /**
     * Username
     */
//    @NotEmpty(message = "用户名不能为空")
    private String username;

    /**
     * Nickname 昵称
     */
    private String nickname;

    /**
     * Password
     */
    private String password;

    /**
     * Salt for encoding
     */
    private String salt;

    /**
     * Sex 性别
     */
    @ManyToOne
    @JoinColumn(name="sexid")
    private Sex sex;

    /**
     * Phone 电话
     */
    private String phone;

    /**
     * Email address.
     *
     * A Email address can be null,but should be correct if exists.
     */
    @Email(message = "请输入正确的邮箱")
    private String email;

    /**
     * Department 部门
     */
    @ManyToOne
    @JoinColumn(name="did")
    private Department department;

    /**
     * Postname 岗位
     */
    @ManyToOne
    @JoinColumn(name="pid")
    private Postname postname;

    /**
     * User status. 状态（正常/停用）
     */
    private boolean enabled;

    /**
     * Remark 备注
     */
    private String remark;

    /**
     * User avatar. 头像
     */
    private String avatar;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;

    /**
     * Transient property for storing role owned by current user. 数据库表中没有这个字段就忽略。
     */
    @Transient
    @JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
    private List<Role> roles;

    // getter and setter
    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }
}
