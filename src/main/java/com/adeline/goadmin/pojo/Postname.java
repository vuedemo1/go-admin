package com.adeline.goadmin.pojo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;

/**
 * Postname 岗位
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "postname")
@JsonIgnoreProperties({"handler","hibernateLazyInitializer"})

public class Postname {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    int id;

    String name;
}
