package com.adeline.goadmin.pojo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.*;

import javax.persistence.*;

/**
 * Sex entity
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "sex")
@JsonIgnoreProperties({"handler","hibernateLazyInitializer"})
public class Sex {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    int id;

    String value;

}
