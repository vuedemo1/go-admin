package com.adeline.goadmin.pojo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.Email;
import java.util.Date;
import java.util.List;

/**
 * Department 部门
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "department")
@JsonIgnoreProperties({"handler","hibernateLazyInitializer"})
public class Department {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    /**
     * 部门名称
     */
    private String name;

    /**
     * 部门树节点 key 值
     */
    private String deptKey;

    /**
     * 显示顺序
     */
    private int deptSort;

    /**
     * 部门负责人
     */
//    @ManyToOne
//    @JoinColumn(name="uid")
//    private User leader;

    /**
     * Phone 电话
     */
    private String phone;

    /**
     * Email address.
     *
     * A Email address can be null,but should be correct if exists.
     */
    @Email(message = "请输入正确的邮箱")
    private String email;

    /**
     * Department status. 状态（正常/停用）
     */
    private boolean enabled;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;

    /**
     * Parent department.
     */
    private int parentId;

    /**
     * Transient property for storing children depts.
     */
    @Transient
    private List<Department> children;

}
