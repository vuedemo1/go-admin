package com.adeline.goadmin.service;

import com.adeline.goadmin.dao.RoleMenuDAO;
import com.adeline.goadmin.pojo.RoleMenu;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoleMenuService {
    @Autowired
    RoleMenuDAO roleMenuDAO;

    List<RoleMenu> findAllByRid(int rid) { return roleMenuDAO.findAllByRid(rid); }
    List<RoleMenu> findAllByRidIn(List<Integer> rids) { return roleMenuDAO.findAllByRidIn(rids); }
}
