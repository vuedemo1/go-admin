package com.adeline.goadmin.service;

import com.adeline.goadmin.dao.UserRoleDAO;
import com.adeline.goadmin.pojo.Role;
import com.adeline.goadmin.pojo.UserRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserRoleService {

    @Autowired
    UserRoleDAO userRoleDAO;

    public List<UserRole> listAllByUid(int uid) {
        return userRoleDAO.findAllByUid(uid);
    }

    /**
     * 如果用户存在，先删除原有用户(uid)对应的所有行，再根据新传递的数据做插入操作
     *
     * @param uid 用户id
     * @param roles 更改后的role
     */
    @Transactional
    public void saveRoleChanges(int uid, List<Role> roles) {
        userRoleDAO.deleteAllByUid(uid);
        List<UserRole> urs = new ArrayList<>();
        roles.forEach(r -> {
            UserRole ur = new UserRole();
            ur.setUid(uid);
            ur.setRid(r.getId());
            urs.add(ur);
        });
        userRoleDAO.saveAll(urs);
    }
}
