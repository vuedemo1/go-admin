package com.adeline.goadmin.service;

import com.adeline.goadmin.dao.LoginlogDAO;
import com.adeline.goadmin.pojo.Loginlog;
import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.JSONObject;
import eu.bitwalker.useragentutils.Browser;
import eu.bitwalker.useragentutils.OperatingSystem;
import eu.bitwalker.useragentutils.UserAgent;
import eu.bitwalker.useragentutils.Version;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.net.*;
import java.nio.charset.Charset;
import java.util.Date;
import java.util.List;

@Service
public class LoginlogService {
    @Autowired
    LoginlogDAO loginlogDAO;

    /**
     * 按id降序列出所有登录日志
     * @return
     */
    public List<Loginlog> listLoginlogs() {
        return loginlogDAO.findAll(Sort.by(Sort.Direction.DESC, "id"));
    }

    /**
     * 按 id 删除登录日志
     * @param id
     */
    public void deleteById(int id) {loginlogDAO.deleteById(id);}

    /**
     * 删除所有登录日志
     */
    public void deleteAll() {loginlogDAO.deleteAll();}

    /**
     * 模糊查询：按登录IP或用户名称查询日志
     * @param ipaddr
     * @param usernmae
     * @return
     */
    public List<Loginlog> search(String ipaddr, String usernmae) {
        List<Loginlog> loginlogs;
        loginlogs = loginlogDAO.findAllByIpaddrLikeAndUsernameLike('%' + ipaddr + '%', '%' + usernmae + '%');
        return loginlogs;
    }

    /**
     * 获得内网IP
     * @return 内网IP
     */
    public static String getIntranetIp() {
        try{
            return InetAddress.getLocalHost().getHostAddress();
        } catch(Exception e){
            throw new RuntimeException(e);
        }
    }

    /**
     * 获取用户ip地址
     * @return
     */
    public static String getIp(HttpServletRequest request){
        // 1. 互联网IP
        String publicipaddress = "";
        try {
            URL url_name = new URL("http://bot.whatismyipaddress.com");
            BufferedReader sc = new BufferedReader(new InputStreamReader(url_name.openStream()));
            // reads system IPAddress
            publicipaddress = sc.readLine().trim();
        } catch (Exception e) {
            e.printStackTrace();
        }
        // 2. 若上一步获取不到互联网IP则通过 request header 查询
        String ip = request.getHeader("x-forwarded-for");
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            // Proxy-Client-IP：apache 服务代理
            ip = request.getHeader("Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            // WL-Proxy-Client-IP：weblogic 服务代理
            ip = request.getHeader("WL-Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            // HTTP_CLIENT_IP：有些代理服务器
            ip = request.getHeader("HTTP_CLIENT_IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("HTTP_X_FORWARDED_FOR");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            //X-Real-IP：nginx服务代理
            ip = request.getHeader("X-Real-IP");
        }
        //有些网络通过多层代理，那么获取到的ip就会有多个，一般都是通过逗号（,）分割开来，并且第一个ip为客户端的真实IP
        if (ip != null && ip.length() != 0) {
            ip = ip.split(",")[0];
        }
        //还是不能获取到，最后再通过request.getRemoteAddr();获取
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getRemoteAddr();
        }
        if (ip.equals("0:0:0:0:0:0:0:1")) {
            ip = "127.0.0.1";
         }
        // 3. 若第1步中获取到互联网IP则直接返回publicipaddress，否则返回通过 request header 查询的IP
        if (publicipaddress != null && !"".equals(publicipaddress)) {
            return publicipaddress;
        } else {
            return ip;
        }
    }

    /**
     * 读取
     *
     * @param rd
     * @return
     * @throws IOException
     */
    private static String readAll(Reader rd) throws IOException {
        StringBuilder sb = new StringBuilder();
        int cp;
        while ((cp = rd.read()) != -1) {
            sb.append((char) cp);
        }
        return sb.toString();
    }

    /**
     * 创建链接
     *
     * @param url
     * @return
     * @throws IOException
     * @throws JSONException
     */
    private static JSONObject readJsonFromUrl(String url) throws IOException, JSONException {
        InputStream is = new URL(url).openStream();
        try {
            BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
            String jsonText = readAll(rd);
            JSONObject json = JSONObject.parseObject(jsonText);
            return json;
        } finally {
            is.close();
        }
    }

    /**
     * 调用百度的ip定位api服务获取登录地理位置
     * @return
     * @throws IOException
     */
    public String getAddress(HttpServletRequest request) throws IOException {
        String address = "已关闭位置获取";
        String ip = getIp(request);
        // 百度地图申请的开发者密钥ak，申请地址：https://lbsyun.baidu.com/apiconsole/key#/home
        String ak = "mRfsh9GdO3ijuQ0d7r9wle4mEMNKpGRV";
        // 这里调用百度的ip定位api服务 详见 https://lbsyun.baidu.com/index.php?title=webapi/ip-api
        JSONObject json = readJsonFromUrl("http://api.map.baidu.com/location/ip?ip="+ip+"&ak="+ak);

        String status = json.getString("status");
        if (status.equals("0")) {
            JSONObject obj2 = (JSONObject) json.get("content");
            address = obj2.getString("address");
        } else if (status.equals("1")) {
            address = "内部IP";
        } else if (status.equals("2")) {
            address = "未知IP";
        }
        return address;
    }

    /**
     * 获取浏览器名称
     */
    public static String getBrowserName(HttpServletRequest request) {
        String header = request.getHeader("User-Agent");
        UserAgent userAgent = UserAgent.parseUserAgentString(header);
        Browser browser = userAgent.getBrowser();
        return browser.getName();
    }

    /**
     * 获取浏览器版本号
     */
    public static String getBrowserVersion(HttpServletRequest request) {
        String header = request.getHeader("User-Agent");
        UserAgent userAgent = UserAgent.parseUserAgentString(header);
        // 获取浏览器信息
        Browser browser = userAgent.getBrowser();
        // 获取浏览器版本号
        Version version = browser.getVersion(header);
        return version.getVersion();
    }

    /**
     * 获取操作系统名称
     */
    public static String getOsName(HttpServletRequest request) {
        String header = request.getHeader("User-Agent");
        UserAgent userAgent = UserAgent.parseUserAgentString(header);
        OperatingSystem operatingSystem = userAgent.getOperatingSystem();
        return operatingSystem.getName();
    }

    /**
     * 将登录日志写入数据库
     * @param request
     * @param username
     * @param massage
     * @throws IOException
     */
    public void writeLoginlogInDB(HttpServletRequest request, String username, String massage, Boolean status) throws IOException {
        Loginlog loginlogInDB = new Loginlog();

        String ipaddr = getIp(request);
        String loginLocation = getAddress(request);
        // 获取的 browserName 自带版本号，如 Chrome 9，因此此处需将自带版本号去除
        String browser = getBrowserName(request).split(" ")[0] + " " + getBrowserVersion(request);
        String os = getOsName(request);

        loginlogInDB.setUsername(username);
        loginlogInDB.setStatus(status);
        loginlogInDB.setIpaddr(ipaddr);
        loginlogInDB.setLoginLocation(loginLocation);
        loginlogInDB.setBrowser(browser);
        loginlogInDB.setOs(os);
        loginlogInDB.setMsg(massage);
        loginlogInDB.setCreateTime(new Date());
        loginlogInDB.setUpdateTime(new Date());

        if (!massage.equals("用户不存在")) {
            loginlogDAO.save(loginlogInDB);
        }
    }

}
