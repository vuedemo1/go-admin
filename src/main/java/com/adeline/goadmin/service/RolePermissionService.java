package com.adeline.goadmin.service;

import com.adeline.goadmin.dao.RoleDAO;
import com.adeline.goadmin.dao.RolePermissionDAO;
import com.adeline.goadmin.pojo.RolePermission;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RolePermissionService {

    @Autowired
    RolePermissionDAO rolePermissionDAO;

    List<RolePermission> findAllByRid(int rid) {return rolePermissionDAO.findAllByRid(rid);}

    List<RolePermission> findAllByRidIn(List<Integer> rids) { return rolePermissionDAO.findAllByRidIn(rids); }
}
