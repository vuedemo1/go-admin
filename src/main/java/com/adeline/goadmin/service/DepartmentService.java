package com.adeline.goadmin.service;

import com.adeline.goadmin.dao.DepartmentDAO;
import com.adeline.goadmin.pojo.Department;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class DepartmentService {

    @Autowired
    DepartmentDAO departmentDAO;

    /**
     * 列出所有部门选项，按id降序排列
     * @return
     */
    public List<Department> listDepartment() {
        return departmentDAO.findAll(Sort.by(Sort.Direction.ASC, "id"));
    }

    public Department getDepartmentById(int id) {
        Department d = departmentDAO.findById(id);
        return d;
    }

    public List<Department> getAllByParentId(int parentId) { return departmentDAO.findAllByParentId(parentId); }

    public List<Department> listDepartmentTree() {
        List<Department> departmentTree = departmentDAO.findAll(Sort.by(Sort.Direction.ASC, "deptSort"));
        handleDepartment(departmentTree);
        return departmentTree;
    }

    /**
     * Adjust the Structure of departments. 要把查询出来的部门数据列表整合成具有层级关系的部门树
     * @param departments Department items list without structure
     */
    public void handleDepartment(List<Department> departments) {
        departments.forEach( department -> {
            List<Department> children = getAllByParentId(department.getId());
            if (children.size() > 0) {
                department.setChildren(children);
            }
        });

        departments.removeIf( department -> department.getParentId() != 0);
    }

    public void deleteById(int id) {
        departmentDAO.deleteById(id);
    }

    /**
     * 根据部门名查找部门是否存在
     * @param name
     * @return
     */
    public boolean isExist(String name) {
        Department department = departmentDAO.findByName(name);
        return null != department;
    }

    /**
     * 查找部门名称是否唯一
     * @param deptName
     * @return
     */
    public boolean isUnique(String deptName) {
        // 数据库中所有部门名列表
        List<String> deptNameList = listDepartment()
                .stream().map(Department::getName).collect(Collectors.toList());
        // 统计正在编辑的部门名在数据库中出现次数
        int count = 0;
        for (String name : deptNameList) {
            if (name.equals(deptName)) {
                count += 1;
            }
        }
        return count == 1;
    }

    /**
     * 修改部门信息
     * @param department
     */
    public void editDept(Department department) {
        Department deptInDB = departmentDAO.findById(department.getId());
        deptInDB.setParentId(department.getParentId());
        deptInDB.setName(department.getName());
        deptInDB.setDeptKey(department.getDeptKey());
        deptInDB.setDeptSort(department.getDeptSort());
        deptInDB.setPhone(department.getPhone());
        deptInDB.setEmail(department.getEmail());
        deptInDB.setEnabled(department.isEnabled());
        deptInDB.setUpdateTime(new Date());

        departmentDAO.save(deptInDB);
    }

    public int addDept(Department department) {
        System.out.println("department" + department);
        Department deptInDB = new Department();
        String deptName = department.getName();

        deptInDB.setParentId(department.getParentId());
        deptInDB.setName(department.getName());
        deptInDB.setDeptKey(department.getDeptKey());
        deptInDB.setDeptSort(department.getDeptSort());
        deptInDB.setPhone(department.getPhone());
        deptInDB.setEmail(department.getEmail());
        deptInDB.setEnabled(department.isEnabled());
        deptInDB.setCreateTime(new Date());
        deptInDB.setUpdateTime(new Date());

        if (deptName.equals("")) return 0;

        boolean exist = isExist(deptName);
        if(exist) return 2;

        departmentDAO.save(deptInDB);
        return 1;
    }

    /**
     * 模糊查询：按照部门名称查询
     * @param deptName
     * @return
     */
    public List<Department> search(String deptName) {
        return departmentDAO.findAllByNameLike('%' + deptName + '%');
    }
}
