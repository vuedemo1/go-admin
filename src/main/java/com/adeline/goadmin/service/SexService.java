package com.adeline.goadmin.service;

import com.adeline.goadmin.dao.SexDAO;
import com.adeline.goadmin.pojo.Sex;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SexService {

    @Autowired
    SexDAO sexDAO;

    /**
     * 列出所有性别选项，按id排序
     * @return
     */
    public List<Sex> listSex() {
        return sexDAO.findAll(Sort.by(Sort.Direction.ASC, "id"));
    }

    public Sex getSexById(int id) {
        Sex s = sexDAO.findById(id).orElse(null);
        return s;
    }
}
