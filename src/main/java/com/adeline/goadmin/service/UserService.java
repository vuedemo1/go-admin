package com.adeline.goadmin.service;

import com.adeline.goadmin.dao.UserDAO;
import com.adeline.goadmin.pojo.Department;
import com.adeline.goadmin.pojo.Role;
import com.adeline.goadmin.pojo.User;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.crypto.SecureRandomNumberGenerator;
import org.apache.shiro.crypto.hash.SimpleHash;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserService {
    @Autowired
    UserDAO userDAO;
    @Autowired
    RoleService roleService;
    @Autowired
    UserRoleService userRoleService;
    @Autowired
    DepartmentService departmentService;

    /**
     * 根据用户名查找用户是否存在
     * @param username
     * @return
     */
    public boolean isExist(String username) {
        User user = userDAO.findByUsername(username);
        return null != user;
    }

    /**
     * 查找用户名是否唯一
     * @param username
     * @return
     */
    public boolean isUnique(String username) {
        // 数据库中所有用户名列表
        List<String> usernameList = listUser()
                .stream().map(User::getUsername).collect(Collectors.toList());
        // 统计正在编辑的用户名在数据库中出现次数
        int count = 0;
        for (String name : usernameList) {
            if (name.equals(username)) {
                count += 1;
            }
        }
        return count == 1;
    }

    /**
     * 列出所有用户及用户角色信息
     * @return
     */
    public List<User> listUser() {
        List<User> users = userDAO.findAll();
        for(User user : users) {
            setRolesByUser(user);
        }
        return users;
    }

    /**
     * 设置用户角色信息
     * @param user
     */
    public void setRolesByUser(User user) {
        List<Role> roles = roleService.listRolesByUserId(user.getId());
        user.setRoles(roles);
    }

    /**
     * 更新用户状态
     * @param user
     */
    public void updateUserStatus(User user) {
        User userInDB = userDAO.findByUsername(user.getUsername());
        userInDB.setEnabled(user.isEnabled());
        userInDB.setUpdateTime(new Date());
        userDAO.save(userInDB);
    }

    public void updateUserAvatar(User user) {
        User userInDB = userDAO.findByUsername(user.getUsername());
        userInDB.setAvatar(user.getAvatar());
        userInDB.setUpdateTime(new Date());
        userDAO.save(userInDB);
    }

    /**
     * 重置用户密码
     * @param user
     * @return
     */
    public User resetPassword(User user) {
        User userInDB = userDAO.findByUsername(user.getUsername());
        String newPassword = user.getPassword();
        // 默认生成 16 位盐
        String salt = new SecureRandomNumberGenerator().nextBytes().toString();
        // 设置 hash 算法迭代次数
        int times = 2;
        userInDB.setSalt(salt);

        String encodedNewPassword = new SimpleHash("md5", newPassword, salt, times).toString();
        userInDB.setPassword(encodedNewPassword);

        userInDB.setUpdateTime(new Date());

        return userDAO.save(userInDB);
    }

    /**
     * 按用户名查找
     * @param username
     * @return
     */
    public User findByUsername(String username) {
        return userDAO.findByUsername(username);
    }

    /**
     * Get current user in DB.
     * @return
     */
    public User getCurrentUser() {
        String username = SecurityUtils.getSubject().getPrincipal().toString();
        User user = findByUsername(username);
        setRolesByUser(user);
        return user;
    }

    /**
     * 按id查找
     * @param id
     * @return
     */
    public User findById(int id) {
        User user = userDAO.findById(id);
        setRolesByUser(user);
        return user;
    }

    /**
     * 按id删除
     * @param id
     */
    public void deleteById(int id) {
        userDAO.deleteById(id);
    }

    /**
     * 按用户名和密码查询用户
     * @param username
     * @param password
     * @return
     */
    public User get(String username, String password) {
        return userDAO.getByUsernameAndPassword(username, password);
    }

    /**
     * 更新用户信息
     * @param user
     */
    public int editUser(User user) {
        User userInDB = userDAO.findById(user.getId());
        userInDB.setNickname(user.getNickname());
        userInDB.setDepartment(user.getDepartment());
        userInDB.setPhone(user.getPhone());
        userInDB.setEmail(user.getEmail());
        userInDB.setUsername(user.getUsername());
        userInDB.setPassword(user.getPassword());
        userInDB.setSex(user.getSex());
        userInDB.setEnabled(user.isEnabled());
        userInDB.setPostname(user.getPostname());
        userInDB.setRoles(user.getRoles());
        userInDB.setRemark(user.getRemark());
        userInDB.setAvatar(user.getAvatar());
        userInDB.setUpdateTime(new Date());

        String username = user.getUsername();
        if (username.equals("")) {
            return 0;
        }

        // 用户名是否唯一
        boolean unique = isUnique(username);
        // 如果用户名存在且不唯一，返回2
        if (!unique) {
            return 2;
        }

        userDAO.save(userInDB);
        userRoleService.saveRoleChanges(userInDB.getId(), user.getRoles());
        return 1;
    }

    /**
     * 添加用户
     * @param user
     */
    public int add(User user) {
        User userInDB = new User();
        String username = user.getUsername();
        String password = user.getPassword();

        userInDB.setNickname(user.getNickname());
        userInDB.setDepartment(user.getDepartment());
        userInDB.setPhone(user.getPhone());
        userInDB.setEmail(user.getEmail());
        userInDB.setUsername(user.getUsername());
        userInDB.setSex(user.getSex());
        userInDB.setEnabled(user.isEnabled());
        userInDB.setPostname(user.getPostname());
        userInDB.setRoles(user.getRoles());
        userInDB.setRemark(user.getRemark());
        userInDB.setAvatar(user.getAvatar());
        userInDB.setCreateTime(new Date());
        userInDB.setUpdateTime(new Date());

        if (username.equals("") || password.equals("")) {
            return 0;
        }

        boolean exist = isExist(username);
        if (exist) {
            return 2;
        }

        // 默认生成 16 位盐
        String salt = new SecureRandomNumberGenerator().nextBytes().toString();
        // 设置 hash 算法迭代次数
        int times = 2;
        String encodedPassword = new SimpleHash("md5", password, salt, times).toString();

        userInDB.setSalt(salt);
        userInDB.setPassword(encodedPassword);

        userDAO.save(userInDB);
        userRoleService.saveRoleChanges(userInDB.getId(), user.getRoles());

        return 1;
    }

    /**
     * 模糊查询：按姓名或电话查找用户。
     * @param
     * @return
     */
    public List<User> search(String username, String phone) {
        List<User> users;
        users = userDAO.findAllByUsernameLikeAndPhoneLike('%' + username + '%', '%' + phone + '%');
        for(User user : users) {
            setRolesByUser(user);
        }
        return users;
    }

    /**
     * 按部门 id 查询
     * @param deptId
     * @return
     */
    public List<User> searchByDeptId(int deptId) {
        List<User> queriedUser = userDAO.findAllByDepartmentId(deptId);
        List<Department> deptChildren = departmentService.getAllByParentId(deptId);
        if(deptChildren != null && deptChildren.size() > 0) {
            deptChildren.forEach( deptChild ->
                queriedUser.addAll(userDAO.findAllByDepartmentId(deptChild.getId()))
            );
        }
        queriedUser.sort(Comparator.comparing(User::getId));
        return queriedUser;
    }
}
