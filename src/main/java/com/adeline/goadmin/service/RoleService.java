package com.adeline.goadmin.service;

import com.adeline.goadmin.dao.RoleDAO;
import com.adeline.goadmin.pojo.Role;
import com.adeline.goadmin.pojo.UserRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class RoleService {

    @Autowired
    RoleDAO roleDAO;
    @Autowired
    UserService userService;
    @Autowired
    UserRoleService userRoleService;

    /**
     * 按显示顺序 roleSort 列出所有角色
     * @return
     */
    public List<Role> listRoles() {
        return roleDAO.findAll(Sort.by(Sort.Direction.ASC, "roleSort"));
    }

    /**
     * 列出用户的所有角色
     * @param id
     * @return
     */
    public List<Role> listRolesByUserId(int id) {
        // 当前用户id
        int uid = id;
        // 查询UserRole表, 找到用户对应的role id列表
        List<Integer> rids = userRoleService.listAllByUid(uid)
                .stream().map(UserRole::getRid).collect(Collectors.toList());
        return roleDAO.findAllById(rids);
    }


    /**
     * 根据角色名查找角色是否存在
     * @param name
     * @return
     */
    public boolean isExist(String name) {
        Role role = roleDAO.findByName(name);
        return null != role;
    }

    /**
     * 查找角色名是否唯一
     * @param rolename
     * @return
     */
    public boolean isUnique(String rolename) {
        // 数据库中所有角色名列表
        List<String> rolenameList = listRoles()
                .stream().map(Role::getName).collect(Collectors.toList());
        // 统计正在编辑的角色名在数据库中出现次数
        int count = 0;
        for (String name : rolenameList) {
            if (name.equals(rolename)) {
                count += 1;
            }
        }
        return count == 1;
    }

    /**
     * 更新角色状态
     * @param role
     */
    public void updateRoleStatus(Role role) {
        Role roleInDB = roleDAO.findByName(role.getName());
        roleInDB.setEnabled(role.isEnabled());
        roleInDB.setUpdateTime(new Date());
        roleDAO.save(roleInDB);
    }

    /**
     * 按id查找
     * @param id
     * @return
     */
    public Role findById(int id) { return roleDAO.findById(id); }

    /**
     * 按id删除
     * @param id
     */
    public void deleteById(int id) { roleDAO.deleteById(id);}

    /**
     * 修改角色信息
     * @param role
     * @return
     */
    public void editRole(Role role) {
        Role roleInDB = roleDAO.findById(role.getId());
//        roleInDB.setName(role.getName());
//        roleInDB.setNameZh(role.getNameZh());
        roleInDB.setRoleSort(role.getRoleSort());
        roleInDB.setRemark(role.getRemark());
        roleInDB.setUpdateTime(new Date());

        roleDAO.save(roleInDB);
    }

    /**
     * 增加角色
     * @param role
     * @return
     */
    public int addRole(Role role) {
        Role roleInDB = new Role();
        String name = role.getName();
        String nameZh = role.getNameZh();

        roleInDB.setName(role.getName());
        roleInDB.setNameZh(role.getNameZh());
        roleInDB.setRoleSort(role.getRoleSort());
        roleInDB.setEnabled(role.isEnabled());
        roleInDB.setRemark(role.getRemark());
        roleInDB.setCreateTime(new Date());
        roleInDB.setUpdateTime(new Date());

        if (name.equals("") || nameZh.equals("")) {
            return 0;
        }

        boolean exist = isExist(name);
        if (exist) {
            return 2;
        }

        roleDAO.save(roleInDB);
        return 1;
    }

    /**
     * 模糊查询：按角色名称或权限字符查询
     * @param name 权限字符
     * @param nameZh 角色名称
     * @return
     */
    public List<Role> search(String name, String nameZh) {
        List<Role> roles;
        roles = roleDAO.findAllByNameLikeAndNameZhLike('%' + name + '%', '%' + nameZh + '%');
        return roles;
    }
}
