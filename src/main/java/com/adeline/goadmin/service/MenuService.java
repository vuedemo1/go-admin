package com.adeline.goadmin.service;

import com.adeline.goadmin.dao.MenuDAO;
import com.adeline.goadmin.pojo.Menu;
import com.adeline.goadmin.pojo.RoleMenu;
import com.adeline.goadmin.pojo.User;
import com.adeline.goadmin.pojo.UserRole;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class MenuService {
    @Autowired
    MenuDAO menuDAO;
    @Autowired
    UserService userService;
    @Autowired
    UserRoleService userRoleService;
    @Autowired
    RoleMenuService roleMenuService;

    public List<Menu> getAllByParentId(int parentId) { return menuDAO.findAllByParentId(parentId); }

    public List<Menu> getMenusByCurrentUser() {
        // Get current user in DB.
        String username = SecurityUtils.getSubject().getPrincipal().toString();
        User user = userService.findByUsername(username);

        // Get roles' ids of current user.
        List<Integer> rids = userRoleService.listAllByUid(user.getId())
                .stream().map(UserRole::getRid).collect(Collectors.toList());

        // Get menu items of these roles.
        List<Integer> menuIds = roleMenuService.findAllByRidIn(rids)
                .stream().map(RoleMenu::getMid).collect(Collectors.toList());
        // 通过stream 来简化列表的处理；使用 map() 提取集合中的某一属性；通过 distinct() 对查询出的菜单项进行去重操作，避免多角色情况下有冗余的菜单。
        List<Menu> menus = menuDAO.findAllById(menuIds).stream().distinct().collect(Collectors.toList());

        // Adjust the structure of the menu.
        handleMenus(menus);
        return menus;
    }

    public List<Menu> getMenusByRoleId(int rid) {
        List<Integer> menuIds = roleMenuService.findAllByRid(rid)
                .stream().map(RoleMenu::getMid).collect(Collectors.toList());
        List<Menu> menus = menuDAO.findAllById(menuIds);

        handleMenus(menus);
        return menus;
    }

    /**
     * Adjust the Structure of the menu. 要把查询出来的菜单数据列表整合成具有层级关系的菜单树
     * @param menus Menu items list without structure
     */
    public void handleMenus(List<Menu> menus) {
        menus.forEach( menu -> {
            List<Menu> children = getAllByParentId(menu.getId());
            menu.setChildren(children);
        });

        menus.removeIf( menu -> menu.getParentId() != 0);
    }
}
