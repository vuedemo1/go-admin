package com.adeline.goadmin.service;

import com.adeline.goadmin.dao.PostnameDAO;
import com.adeline.goadmin.pojo.Postname;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PostnameService {

    @Autowired
    PostnameDAO postnameDAO;

    /**
     * 列出所有岗位选项，按id降序排列
     * @return
     */
    public List<Postname> listPostname() {
        return postnameDAO.findAll(Sort.by(Sort.Direction.ASC, "id"));
    }

    public Postname getPostnameById(int id) {
        Postname p = postnameDAO.findById(id).orElse(null);
        return p;
    }
}
