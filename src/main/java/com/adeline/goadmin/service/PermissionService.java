package com.adeline.goadmin.service;

import com.adeline.goadmin.dao.PermissionDAO;
import com.adeline.goadmin.dao.RolePermissionDAO;
import com.adeline.goadmin.dao.UserDAO;
import com.adeline.goadmin.dao.UserRoleDAO;
import com.adeline.goadmin.pojo.Permission;
import com.adeline.goadmin.pojo.Role;
import com.adeline.goadmin.pojo.RolePermission;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class PermissionService {

    @Autowired
    PermissionDAO permissionDAO;

    @Autowired
    RolePermissionDAO rolePermissionDAO;

    @Autowired
    UserRoleService userRoleService;

    @Autowired
    RoleService roleService;

    @Autowired
    UserDAO userDAO;

    public List<Permission> list() {return permissionDAO.findAll();}

    public boolean needFilter(String requestAPI) {
        List<Permission> perms = permissionDAO.findAll();
        for (Permission p : perms) {
            // match prefix
            if (requestAPI.startsWith(p.getUrl())) {
                return true;
            }
        }
        return false;
    }

    /**
     * 根据角色id列出角色的所有权限
     * @param rid
     * @return
     */
    public List<Permission> listPermsByRoleId(int rid) {
        List<Integer> pids = rolePermissionDAO.findAllByRid(rid)
                .stream().map(RolePermission::getPid).collect(Collectors.toList());
        return permissionDAO.findAllById(pids);
    }

    public Set<String> listPermissionURLsByUser(String username) {
        int uid = userDAO.findByUsername(username).getId();
        List<Integer> rids = roleService.listRolesByUserId(uid)
                .stream().map(Role::getId).collect(Collectors.toList());

        List<Integer> pids = rolePermissionDAO.findAllByRidIn(rids)
                .stream().map(RolePermission::getPid).collect(Collectors.toList());

        List<Permission> perms = permissionDAO.findAllById(pids);

        Set<String> URLs = perms.stream().map(Permission::getUrl).collect(Collectors.toSet());

        return URLs;
    }
}
