package com.adeline.goadmin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GoAdminApplication {

    public static void main(String[] args) {
        SpringApplication.run(GoAdminApplication.class, args);
    }

}
